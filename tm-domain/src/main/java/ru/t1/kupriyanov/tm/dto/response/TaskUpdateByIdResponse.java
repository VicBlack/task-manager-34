package ru.t1.kupriyanov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.model.Task;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(@Nullable Task task) {
        super(task);
    }

}
