package ru.t1.kupriyanov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectUpdateByIdRequest extends AbstractIdRequest {

    @Nullable
    private String name;

    @Nullable
    private String description;

    public ProjectUpdateByIdRequest(
            @Nullable String token,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    ) {
        super(token, id);
        this.name = name;
        this.description = description;
    }

}
