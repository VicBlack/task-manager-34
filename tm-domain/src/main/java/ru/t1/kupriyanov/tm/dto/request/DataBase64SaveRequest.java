package ru.t1.kupriyanov.tm.dto.request;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
public final class DataBase64SaveRequest extends AbstractUserRequest {

    public DataBase64SaveRequest(@Nullable String token) {
        super(token);
    }

}
