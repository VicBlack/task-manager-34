package ru.t1.kupriyanov.tm.dto.request;

import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.enumerated.TaskSort;

public final class TaskListRequest extends AbstractUserRequest {

    @Nullable
    private TaskSort sort;

    public TaskListRequest(@Nullable String token, @Nullable TaskSort sort) {
        super(token);
        this.sort = sort;
    }

}
