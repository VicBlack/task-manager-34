package ru.t1.kupriyanov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.DataJsonSaveJaxBRequest;
import ru.t1.kupriyanov.tm.enumerated.Role;

public final class DataJsonSaveJaxBCommand extends AbstractDataCommand {

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[SAVE JSON DATA]");
        getDomainEndpoint().saveDataJsonJaxB(new DataJsonSaveJaxBRequest(getToken()));
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to a .json file.";
    }

    @Override
    @NotNull
    public  String getName() {
        return "save-json-data";
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
