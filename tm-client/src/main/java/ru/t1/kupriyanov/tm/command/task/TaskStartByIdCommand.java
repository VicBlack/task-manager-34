package ru.t1.kupriyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.TaskChangeStatusByIdRequest;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class TaskStartByIdCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final TaskChangeStatusByIdRequest request =
                new TaskChangeStatusByIdRequest(getToken(), id, Status.IN_PROGRESS);
        getTaskEndpoint().changeTaskStatusByIdRequest(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by id.";
    }

}
