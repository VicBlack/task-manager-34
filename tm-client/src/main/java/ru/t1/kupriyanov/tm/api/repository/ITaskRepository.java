package ru.t1.kupriyanov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.kupriyanov.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    List<Task> findAllByProjectId(@NotNull String userId, @NotNull String projectId);

}