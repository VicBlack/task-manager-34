package ru.t1.kupriyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.TaskGetByIndexRequest;
import ru.t1.kupriyanov.tm.dto.response.TaskGetByIndexResponse;
import ru.t1.kupriyanov.tm.model.Task;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskGetByIndexRequest request = new TaskGetByIndexRequest(getToken(), index);
        @NotNull final TaskGetByIndexResponse response = getTaskEndpoint().getTaskByIndex(request);
        showTask(response.getTask());
    }

    @NotNull
    @Override
    public String getName() {
        return "task-show-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show task by index.";
    }

}
