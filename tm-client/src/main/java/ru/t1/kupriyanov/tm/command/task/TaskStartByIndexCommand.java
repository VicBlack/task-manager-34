package ru.t1.kupriyanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.TaskChangeStatusByIndexRequest;
import ru.t1.kupriyanov.tm.enumerated.Status;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX: ");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskChangeStatusByIndexRequest request =
                new TaskChangeStatusByIndexRequest(getToken(), index, Status.IN_PROGRESS);
        getTaskEndpoint().changeTaskStatusByIndexRequest(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "task-start-by-index";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Start task by index.";
    }

}
