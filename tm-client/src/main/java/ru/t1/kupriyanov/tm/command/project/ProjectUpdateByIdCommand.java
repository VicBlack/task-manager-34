package ru.t1.kupriyanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kupriyanov.tm.dto.request.ProjectUpdateByIdRequest;
import ru.t1.kupriyanov.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractProjectCommand {

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY ID]");
        System.out.println("ENTER ID: ");
        @Nullable final String id = TerminalUtil.nextLine();
        System.out.println("ENTER NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIdRequest request =
                new ProjectUpdateByIdRequest(getToken(), id, name, description);
        getProjectEndpoint().updateProjectById(request);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-update-by-id";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update project by id.";
    }

}
